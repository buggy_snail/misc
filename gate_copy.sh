#!/bin/sh

workspace_path=~/tmp/gate
gate_name=vyborg_gate

target_path=~/tmp/gate_cross


echo  "Copy gate $workspace_path/$gate_name/ to $target_path/"


if [ ! -d "$target_path" ]
then
mkdir "$target_path"
fi


# ==============================================
name=$gate_name

dst="$target_path/$name"
src="$workspace_path/$name"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

filename=main.cpp; cp "$src/$filename" "$dst/$filename"
filename=switch_msg.cpp; cp "$src/$filename" "$dst/$filename"
filename=switch_msg.h; cp "$src/$filename" "$dst/$filename"
# ==============================================

# ==============================================
name=module_vyborg

dst="$target_path/$name"
src="$workspace_path/$name"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

filename=logic.cpp; cp "$src/$filename" "$dst/$filename"
filename=logic.h; cp "$src/$filename" "$dst/$filename"
filename=main.cpp; cp "$src/$filename" "$dst/$filename"
# ==============================================

# ==============================================
name=ModbusTCPClient

dst="$target_path/$name"
src="$workspace_path/$name"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

filename=ModbusTCPClient.cpp; cp "$src/$filename" "$dst/$filename"
filename=ModbusTCPClient.h; cp "$src/$filename" "$dst/$filename"
# ==============================================

# ==============================================
name=ModbusDataUnit

dst="$target_path/$name"
src="$workspace_path/$name"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

filename=ModbusDataUnit.cpp; cp "$src/$filename" "$dst/$filename"
filename=ModbusDataUnit.h; cp "$src/$filename" "$dst/$filename"
# ==============================================

# ==============================================
name=GateMessages

dst="$target_path/$name"
src="$workspace_path/$name"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

filename=GateMessages.h; cp "$src/$filename" "$dst/$filename"
# ==============================================

# ==============================================
name=GateConnectServer

dst="$target_path/$name"
src="$workspace_path/$name"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

filename=GateConnectServer.cpp; cp "$src/$filename" "$dst/$filename"
filename=GateConnectServer.h; cp "$src/$filename" "$dst/$filename"
# ==============================================

# ==============================================
name=GateConnect

dst="$target_path/$name"
src="$workspace_path/$name"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

filename=GateConnect.cpp; cp "$src/$filename" "$dst/$filename"
filename=GateConnect.h; cp "$src/$filename" "$dst/$filename"
# ==============================================



# ========== Makefile ==========================
BASEDIR=$(dirname $0)
cp "$BASEDIR/$Makefile_cross" "$target_path/Makefile"
# ==============================================


echo "Done"



