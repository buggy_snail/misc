#!/bin/sh

workspace_path=~/my_repos/gate
gate_name=gate_ustkut

target_path=~/projects/gate_ustkut
lib_iec60870_path=~/lib60870_cross/build

echo  "Copy gate $workspace_path/$gate_name/ to $target_path/"


if [ ! -d "$target_path" ]
then
mkdir "$target_path"
fi


# ==============================================
name=$gate_name

dst="$target_path/$name"
src="$workspace_path/$name"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

filename=main.cpp; cp "$src/$filename" "$dst/$filename"
filename=switch_msg.cpp; cp "$src/$filename" "$dst/$filename"
filename=switch_msg.h; cp "$src/$filename" "$dst/$filename"
# ==============================================

# ==============================================
name=module_ustkut

dst="$target_path/$name"
src="$workspace_path/$name"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

filename=main.cpp; cp "$src/$filename" "$dst/$filename"
filename=logic.cpp; cp "$src/$filename" "$dst/$filename"
filename=logic.h; cp "$src/$filename" "$dst/$filename"
filename=crc32.cpp; cp "$src/$filename" "$dst/$filename"
filename=crc32.h; cp "$src/$filename" "$dst/$filename"
filename=param_tree.cpp; cp "$src/$filename" "$dst/$filename"
filename=param_tree.h; cp "$src/$filename" "$dst/$filename"
filename=iec60870.cpp; cp "$src/$filename" "$dst/$filename"
filename=iec60870.h; cp "$src/$filename" "$dst/$filename"
filename=common.h; cp "$src/$filename" "$dst/$filename"
filename=common.cpp; cp "$src/$filename" "$dst/$filename"
filename=journal.h; cp "$src/$filename" "$dst/$filename"
filename=journal.cpp; cp "$src/$filename" "$dst/$filename"
filename=oscilloscope.h; cp "$src/$filename" "$dst/$filename"
filename=oscilloscope.cpp; cp "$src/$filename" "$dst/$filename"
filename=settings.h; cp "$src/$filename" "$dst/$filename"
filename=settings.cpp; cp "$src/$filename" "$dst/$filename"
filename=utf8.h; cp "$src/$filename" "$dst/$filename"
filename=delta_hmi.h; cp "$src/$filename" "$dst/$filename"
filename=delta_hmi.cpp; cp "$src/$filename" "$dst/$filename"
filename=sql_connect.h; cp "$src/$filename" "$dst/$filename"
filename=sql_connect.cpp; cp "$src/$filename" "$dst/$filename"
# ==============================================

# ==============================================
name=GateMessages

dst="$target_path/$name"
src="$workspace_path/$name"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

filename=GateMessages.h; cp "$src/$filename" "$dst/$filename"
# ==============================================

# ==============================================
name=GateConnectServer

dst="$target_path/$name"
src="$workspace_path/$name"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

filename=GateConnectServer.cpp; cp "$src/$filename" "$dst/$filename"
filename=GateConnectServer.h; cp "$src/$filename" "$dst/$filename"
# ==============================================

# ==============================================
name=GateConnect

dst="$target_path/$name"
src="$workspace_path/$name"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

filename=GateConnect.cpp; cp "$src/$filename" "$dst/$filename"
filename=GateConnect.h; cp "$src/$filename" "$dst/$filename"
# ==============================================



# ==============================================
name=lib60870

dst="$target_path/$name"
src="$lib_iec60870_path"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

#filename=lib60870.a; cp "$src/$filename" "$dst/$filename"
filename=lib60870.so; cp "$src/$filename" "$dst/$filename"
# ==============================================


# ==============================================
name=module_iec60870

dst="$target_path/$name"
src="$workspace_path/$name"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

filename=main.cpp; cp "$src/$filename" "$dst/$filename"
# ==============================================


# ==============================================
name=module_surza

dst="$target_path/$name"
src="$workspace_path/$name"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

filename=main.cpp; cp "$src/$filename" "$dst/$filename"
filename=surza_datetime.cpp; cp "$src/$filename" "$dst/$filename"
filename=surza_datetime.h; cp "$src/$filename" "$dst/$filename"
# ==============================================


# ==============================================
name=ModbusDataUnit

dst="$target_path/$name"
src="$workspace_path/$name"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

filename=ModbusDataUnit.cpp; cp "$src/$filename" "$dst/$filename"
filename=ModbusDataUnit.h; cp "$src/$filename" "$dst/$filename"
# ==============================================


# ==============================================
name=GateCommon

dst="$target_path/$name"
src="$workspace_path/$name"
if [ ! -d $dst ]
then
mkdir "$dst"
fi

filename=gate_common.cpp; cp "$src/$filename" "$dst/$filename"
filename=gate_common.h; cp "$src/$filename" "$dst/$filename"
# ==============================================


# ========== Makefile ==========================
BASEDIR=$(dirname $0)
cp "$BASEDIR/Makefile_ustkut_cross" "$target_path/Makefile"
# ==============================================




echo "Done"


